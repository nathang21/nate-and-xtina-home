## Nate and Xtina Smarthome

Contains:
- Hassio on a RPI4B
- Docker Pihole on a RPI3B+

## Config
Edit the `configuration.yaml` file

### IPV6

1. ssh to supervisor
1. View Configs: `nmcli c s`
1. Edit each config: `nmcli c edit ${NAME}`
1. Run: 
```
set ipv6.addr-gen-mode eui64
save
quit
```
Based on [these steps](https://community.home-assistant.io/t/add-ipv6-network-configuration-to-disable-privacy-extensions/369599/5?u=nathang21)


## Deploy
This hassio configuration will automatically pull changes from master via the Git Pull addon. A simple `git push` is all that is needed.

NOTE: For secrets, since `secrets.yaml` is excluded, you need to locally edit the `secrets.yaml` file to add new variables either via the web UI, SSH, or console.'

Check the Git Pull addon logs for errors on the config, if succesfully it will auto-restart and apply the changes. 

## Restore/Fresh Install

## Home Assistant
1. Flash SD Card
1. Boot, go through HA onboarding
1. Add repo and install Addon: https://github.com/sabeechen/hassio-google-drive-backup
1. Follow steps to restore: https://github.com/sabeechen/hassio-google-drive-backup#how-do-i-restore-a-snapshot
